import Promise from 'bluebird';
import {details, assetS3Key} from 'namer';
import fetchAndHydrate from 'trading-feature-creator';
import {getPrediction} from './src/ml'
import {write} from './src/utils/s3';
import {handleTopicArgs} from './src/utils/helpers';
import {convertPredictionToFirebase} from './src/utils/formatter';
import { savePredictions } from 'firebase';

module.exports.forecastEndpoint = (rawEvent, context, cb) => {
  const event = handleTopicArgs(rawEvent);
  
  const {ticker, period, klass, id} = event;
  if (!ticker ) return cb('ticker param is needed');
  if (!klass ) return cb('klass param is needed');
  if (!id ) return cb('id param is needed');
  const purpose = 'forecast';

  const namer = details(id, klass);
  const key = assetS3Key(ticker, purpose, namer);
  let data;
  let quote;


  const features = fetchAndHydrate(ticker, period, purpose, namer);

  features.then( (res) => {
    return new Promise( (resolve, reject) => {
      const obj = {};
      Object.keys(res[0]).forEach( (k) => {
        if (k !== 'change') {
          obj[k.toString()] = res[0][k].toString();
        }
      });
      resolve(obj);
    });
  }).then( (formatted) => {
    console.log('res', formatted);
    quote = formatted;
    return getPrediction(formatted, null, namer);
  })

  // TODO: only save it if it is forecast to go up or down? To take action.
  .then( (prediction) => {
    return convertPredictionToFirebase(ticker, prediction, quote);
  })
  .then( (formatted) => {
    const {res, Date} = formatted;
    return savePredictions(namer.MLModelId, res, Date);
  })
  .then( (res) => {
    cb(null, res);
  })
  .catch( (err) => {
    cb(err);
  });
};
