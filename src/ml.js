import AWS from 'aws-sdk';

const machinelearning = new AWS.MachineLearning({apiVersion: '2014-12-12', region: 'us-east-1'});

const DEFAULT_ENDPOINT = 'https://realtime.machinelearning.us-east-1.amazonaws.com';

export const getPrediction = (features, endpoint = DEFAULT_ENDPOINT, namer) => {
  return new Promise((resolve, reject) => {
    const params = {
      MLModelId: namer.MLModelId,
      PredictEndpoint: DEFAULT_ENDPOINT,
      Record: features
    };
    machinelearning.predict(params, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      };
    });
  })
  .catch( (err) => {
    console.info('an error occurred', err);
  });
}
