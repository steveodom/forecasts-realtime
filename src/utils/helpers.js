export const handleTopicArgs = (event) => {
  if (event && event.Records) {
    const params = event.Records[0].Sns.MessageAttributes;
    const obj = {};
    Object.keys(params).forEach( (key) => {
      obj[key] = params[key].Value;    
    });
    return obj;
  } else {
    return event;
  }
}
