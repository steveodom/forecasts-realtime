import json2csv from 'json2csv';
import Promise from 'bluebird';

export const convertQuotesToCSV = (quotes) => {
  const fields = Object.keys(quotes[0]);
  // addd hasCSVColumnTitle: false if you don't want a header line
  return json2csv({ data: quotes, fields: fields, hasCSVColumnTitle: false});
}

export const convertPredictionToFirebase = (ticker, prediction, quote) => {
  try {
    const {Date, intradayIndex, ticksToEndOfDay} = quote;
    const score = prediction.Prediction.predictedScores['0'];
    const res = [
      {ticker: ticker, score, intradayIndex, ticksToEndOfDay } 
    ];
    return Promise.resolve({Date, res});
  } catch(err) {
    return Promise.reject({err: err});
  }
  
}
