import {expect} from 'chai';
import { convertPredictionToFirebase } from './../../src/utils/formatter';

describe('Formatters - convertPrediction', () => {
  it('should return an object with Date and res keys', () => {
    const prediction = { 
      Prediction:{ 
        predictedLabel: '0',
        predictedScores: { '0': 0.00500029930844903 },
      } 
    };
    const ticker = 'AAPL';

    const quote = { 
      Timestamp: '1481305920',
      close: '114.06',
      Date: 'daily-12-09-2016-11:52',
      date: 'Dec 09 2016 11:52',
      intradayIndex: '40',
      ticksToEndOfDay: '38'
    }
    return convertPredictionToFirebase(ticker, prediction, quote).then( (res) => {
      expect(typeof res).to.equal('object');
      expect(res).to.have.property('Date');
      return expect(res).to.have.property('res');
    });
  });

  it('should return an error object if something is missing', () => {
    const prediction = null;
    const ticker = 'AAPL';

    const quote = { 
      Timestamp: '1481305920',
      close: '114.06',
      Date: 'daily-12-09-2016-11:52',
      date: 'Dec 09 2016 11:52',
      intradayIndex: '40',
      ticksToEndOfDay: '38'
    }
    return convertPredictionToFirebase(ticker, prediction, quote).then( (res) => {
      return true;
    }).catch( (err) => {
      return expect(err).to.have.property('err');
    });
  });
});